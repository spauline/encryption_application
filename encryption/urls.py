from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'encryption_app.views.index_page', name='encrypt_text'),
    url(r'^text_transformation/', 'encryption_app.views.text_transformation', name='text_transformation'),
    url(r'^tip_handling/', 'encryption_app.views.tip_handling', name='tip_handling'),

)
