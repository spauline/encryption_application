from __future__ import division
from django.utils.datastructures import SortedDict
import enchant
import nltk
import string
import re

lower = list(string.lowercase)
upper = list(string.uppercase)
alphabet = len(lower)


def encrypt_text(initial_text, key):
    encrypted_text = str()
    for character in initial_text:
        if character in lower:
            encrypted_text += lower[(lower.index(character) + key) % alphabet]
        elif character in upper:
            encrypted_text += upper[(upper.index(character) + key) % alphabet]
        else:
            encrypted_text += character
    return encrypted_text


def decrypt_text(initial_text, key):
    decrypted_text = str()
    for character in initial_text:
        if character in lower:
            decrypted_text += lower[(lower.index(character) - key + alphabet) % alphabet]
        elif character in upper:
            decrypted_text += upper[(upper.index(character) - key + alphabet) % alphabet]
        else:
            decrypted_text += character
    return decrypted_text


def count_letters(initial_text):
    result = dict()
    for character in initial_text:
        if character.upper() in upper:
            if character.upper() not in result:
                result[character.upper()] = 1
            else:
                result[character.upper()] += 1
    return SortedDict(result)


def analyze_text(initial_text):
    dictionary = enchant.Dict("en_US")
    words = nltk.word_tokenize(initial_text)
    words_amount = 0
    real_words_array = re.compile('\w+').findall(initial_text)
    for word in words:
        if dictionary.check(word) and len(word) > 1:
            words_amount += 1
            real_words_array.append(word)
    real_words = False if words_amount / len(words) < 0.5 else True
    recommended_key = None
    if not real_words:
        for n in xrange(1, 26):
            if dictionary.check(decrypt_text(real_words_array[0], n)) and dictionary.check(
                    decrypt_text(real_words_array[-1], n)) and \
                            len(real_words_array[0]) > 1 and len(real_words_array[-1]) > 1:
                recommended_key = n
    if real_words and recommended_key is None:
        return ""
    elif not real_words and recommended_key is None:
        return "It seems that you entered invalid text"
    elif not real_words and recommended_key is not None:
        return "It is recommended to use the following Caesar's code for decrypting: " + str(recommended_key)
