# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Encryption',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('initial_text', models.TextField(max_length=500)),
                ('transformed_text', models.TextField(max_length=500, blank=True)),
                ('key', models.IntegerField(blank=True)),
            ],
        ),
    ]
