# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('encryption_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='encryption',
            name='key',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
