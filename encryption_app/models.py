from django.db import models


class Encryption(models.Model):
    initial_text = models.TextField(max_length=500)
    transformed_text = models.TextField(max_length=500, blank=True)
    key = models.IntegerField(blank=True, null=True)




