from django.http import HttpResponse
from django.shortcuts import render
import simplejson as json


from encryption_app import models

from functions import *


def text_transformation(request):
    input_text = request.GET.get("text")
    key = request.GET.get("key")
    transformation_type = request.GET.get("transformation")
    encrypt_object = models.Encryption.objects.create(initial_text=input_text, key=int(key))
    if transformation_type == "encrypt":
        encrypt_object.transformed_text = encrypt_text(encrypt_object.initial_text, encrypt_object.key)
    elif transformation_type == "decrypt":
        encrypt_object.transformed_text = decrypt_text(encrypt_object.initial_text, encrypt_object.key)
    encrypt_object.save()
    counted_letters = count_letters(encrypt_object.initial_text)
    data = json.dumps({'result_text': encrypt_object.transformed_text, 'counted_letters': counted_letters})
    return HttpResponse(data, content_type='application/json; charset=utf-8')


def index_page(request):
    return render(request, 'index.html', {'code_numbers': range(1, 26)})


def tip_handling(request):
    text = request.GET.get("text")
    data = json.dumps({'msg': analyze_text(text)})
    return HttpResponse(data, content_type='application/json; charset=utf-8')
