var typingTimer;
var doneTypingInterval = 1000;
$(document).ready(function () {
    $('#inputTextarea').keypress(function () {

        $('#chartContainer').css({
            display: 'none'
        });
        $('#tipContainer').css({
            display: 'none'
        })

    })
        .keyup(function () {
            $('#resultTextarea').val("");
            clearTimeout(typingTimer);
            if ($('#inputTextarea').val) {
                typingTimer = setTimeout(function () {

                    getTip($('#inputTextarea').val());

                }, doneTypingInterval);
            }
        });


});


var getTip = function (text_area_value) {
    $.getJSON("/tip_handling", {text: text_area_value}, function (data) {
        var msg = data['msg'];
        if (msg.length > 0) {
            $("#tip").text(msg);
            $('#tipContainer').css({
                display: 'block'
            })
        }


    });
};

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

var encryptText = function () {
    $('#tipContainer').css({
        display: 'none'
    });
    var inputText = $('#inputTextarea').val();
    var key = $('#encryptKey').val();
    var transformationType = "";
    if ($("#encryptRadio").is(":checked")) {
        transformationType = "encrypt";

    }
    else {
        transformationType = "decrypt";
    }


    $.getJSON("/text_transformation", {text: inputText, key: key, transformation: transformationType}, function (data) {
        $('#resultTextarea').val(data['result_text']);
        var countedLetters = data['counted_letters'];
        var chartData = [];
        var i;
        for (i = 0; i < Object.keys(countedLetters).length; i++) {
            chartData[i] = {
                "label": Object.keys(countedLetters)[i],
                "y": countedLetters[Object.keys(countedLetters)[i]]
            }
        }
        $('#chartContainer').css({
            display: ''
        });
        var chart = new CanvasJS.Chart("chartContainer", {
            title: {
                text: "Occurrences of every letter in your text",
                fontFamily: "OpenSansRegular, sans-serif",
                fontWeight: "lighter",
                fontColor: "#222222",
                fontSize: 30
            },
            data: [
                {


                    type: "column",
                    dataPoints: chartData
                }
            ]

        });

        chart.render();

    })


};

